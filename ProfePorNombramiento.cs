﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    class ProfePorNombramiento:Profe
    {
        public double Sueldo = 800;
        public double HorasEX { get; set; }
        public override void Psueldo()
        {
            double Sueldo = this.Sueldo + HorasEX;
            Console.WriteLine("EL Sueldo " + Sueldo);
        }
        public override void CambiarDepartamento(string Departamento)
        {
            this.Departamento = Departamento;
        }

        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Las Horas Extras: {0}", HorasEX);
        }
    }
}
