﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    public class ProfePorContrato:Profe
    
    {
        public int Ttrabajado = 10;
        public double SueldoPorHora = 4.79;

        public override void Psueldo()
        {
            double sueldo = SueldoPorHora * Ttrabajado;
            Console.WriteLine("Sueldo" + sueldo);
        }

        public override void CambiarDepartamento(string Departamento)
        {
            this.Departamento = Departamento;
        }
        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Horas trabajadas: {0}", Ttrabajado);
        }

    }
}
