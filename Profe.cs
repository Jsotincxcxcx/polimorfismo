﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    public abstract class Profe:Empleado
    {
        public string Departamento { get; set; }

        public abstract void CambiarDepartamento(string Departamento);
        public override void ElDespacho(int despacho)
        {
            this.despacho = despacho;
        }
        public abstract void Psueldo();

        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Profesor\nDepartamento: {0}", Departamento);
        }
    }
}
