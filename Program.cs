﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    class Program
    {
        static void Main(string[] args)
        {
            PersonalDeServicio personaDeServicio = new PersonalDeServicio();
            personaDeServicio.Nombre = "Carlos";
            personaDeServicio.Apellido = "Bajaña";
            personaDeServicio.Cedula = "1315900363";
            personaDeServicio.EstadoCivil = "divorciado";
            personaDeServicio.Incorporacion = new DateTime(2019, 08, 2);
            personaDeServicio.seccion = "Sistemas";
            personaDeServicio.despacho = 10;

            ProfePorContrato persona = new ProfePorContrato();
            persona.Nombre = "Brandom";
            persona.Apellido = "Palacios";
            persona.Cedula = "1661425778";
            persona.EstadoCivil = "Soltero";
            persona.Incorporacion = new DateTime(2015, 10, 3);
            persona.despacho = 9;
            persona.Departamento = "Biblioteca";

            List<Persona> personas = new List<Persona>();
            personas.Add(personaDeServicio);
            personas.Add(persona);
            foreach (Persona inte in personas)
            {
                inte.MostrarDatos();
            }
            Console.ReadKey(); ;
        }
    }
}
