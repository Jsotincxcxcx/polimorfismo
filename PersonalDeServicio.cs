﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    class PersonalDeServicio: Empleado
    {
        public string seccion { get; set; }
        public override void ElDespacho(int despacho)
        {
            this.despacho = despacho;

        }

        public void Cambiarseccion(string seccion)
        {
            this.seccion = seccion;
        }

        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Personal de Servicio\nSeccion: {0}", seccion);
        }

    }
}
