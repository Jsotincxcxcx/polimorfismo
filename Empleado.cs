﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    public abstract class Empleado:Persona
    {
        public DateTime Incorporacion { get; set; }
        public int despacho { get; set; }

        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Fecha De Ingreso: " + Incorporacion + "Numero de despacho:" + despacho);
        }
        public override void CambioDeEstado(string EstadoCivil)
        {
            this.EstadoCivil = EstadoCivil;
        }
        public abstract void ElDespacho(int despacho);
    }
}
