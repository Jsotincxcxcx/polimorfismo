﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    public abstract class Persona
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Cedula { get; set; }
        public string EstadoCivil { get; set; }


        public virtual void MostrarDatos()
        {
            Console.WriteLine("Nombre y Apellido: {0} \n {1};Cedula: \n {2};Estado Civil:\n {3}", Nombre, Apellido, Cedula, EstadoCivil);
        }

        public abstract void CambioDeEstado(string EstadoCivil);

    }

}