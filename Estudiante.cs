﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
     class Estudiante : Persona

    {
        public int matricula { get; set; }

        public void Matriculacion(int Curso)
        {
            matricula = Curso;
        }

        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Estudiante\nCurso Matriculado: {0}", matricula);
        }

        public override void CambioDeEstado(string Estadocivil)
        {
            this.EstadoCivil = Estadocivil;
        }
    }
}
